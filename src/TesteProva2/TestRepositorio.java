package TesteProva2;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TestRepositorio {

	private RepositorioTema umTemaControle;
	private Tema umTema;
	
	@Before
	public void setUp()throws Exception{
		umTemaControle = new RepositorioTema();
		umTema = new Tema();
	}

	@Test
	public void testAdicionar() {		
		assertEquals("Tema adicionado com sucesso!", umTemaControle.adicionar(umTema));
		
	}
	
	@Test
	public void testRemover() {		
		assertEquals("Tema removido com sucesso!", umTemaControle.remover(umTema));
		
	}
	
	@Test
	public void testPesquisar() {
		String umNome = "Copa";
		umTema.setNome(umNome);
		umTemaControle.adicionar(umTema);
		
		assertEquals(umTema, umTemaControle.pesquisar(umNome));
		
	}

}
