package TesteProva2;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TesteUsuario {

	private Usuario umUsuario;
	
	@Before
	public void setUp()throws Exception{
		umUsuario = new Usuario();
	}
	
	@Test
	public void testNome() {
		umUsuario.setNome("Matheus");
		assertEquals("Matheus", umUsuario.getNome());
		
	}
	
	@Test
	public void testId() {
		umUsuario.setId("Matheus_oo");
		assertEquals("Matheus_oo", umUsuario.getId());
		
	}
	
}
